# ElevatorSystem

# Details:
This is a sample project which contains the simulation for a Elevator System.

# Hardware
1. RAM : 2 GB and Above
2. HDD : 64 GB
3. OS  : Mac/Linux/Window


# Technology Stack
1. Java 8


# How to run the code
 Please find a sample driver code for running the code. 

Public class ElevatorDriver {

    public static void main(String[] args) throws InterruptedException {
        boolean runFlag = true;
        Elevator e1 = new Elevator();
        Floor f1 = new Floor(1,e1);
        Floor f2 = new Floor(2,e1);
        Floor f3 = new Floor(3,e1);
        Floor f4 = new Floor(4,e1);

        e1.setCurrentFloor(f1);
        ElevatorService es = new ElevatorServiceImpl(e1);
        Thread.currentThread().sleep(20);
        es.callALift(f2);
        es.callALift(f3);
        es.callALift(f4);
        es.callALift(f1);
        Thread.currentThread().sleep(10000);

        es.callALift(f2);
        es.callALift(f3);
        es.callALift(f1);

    }


