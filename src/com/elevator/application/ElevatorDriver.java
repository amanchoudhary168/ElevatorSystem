package ElevatorSystem.application;

import ElevatorSystem.entities.Elevator;
import ElevatorSystem.entities.Floor;
import ElevatorSystem.services.ElevatorService;
import ElevatorSystem.services.ElevatorServiceImpl;

import java.util.Scanner;

public class ElevatorDriver {

    public static void main(String[] args) throws InterruptedException {
        boolean runFlag = true;
        Elevator e1 = new Elevator();
        Floor f1 = new Floor(1,e1);
        Floor f2 = new Floor(2,e1);
        Floor f3 = new Floor(3,e1);
        Floor f4 = new Floor(4,e1);

        e1.setCurrentFloor(f1);
        ElevatorService es = new ElevatorServiceImpl(e1);
        Thread.currentThread().sleep(20);
        es.callALift(f2);
        es.callALift(f3);
        es.callALift(f4);
        es.callALift(f1);
        Thread.currentThread().sleep(10000);

        es.callALift(f2);
        es.callALift(f3);
        es.callALift(f1);








        /*while(runFlag){
            printOptionsList();
            Scanner scan = new Scanner(System.in);
            int choice = scan.nextInt();
            if(choice == 1)
                callLiftOptions();
                int choice2 = scan.nextInt();

            break;
        }*/


    }

    public static void  printOptionsList(){
        System.out.println("Choose between following options :");
        System.out.println("1. Call A Lift " );
        System.out.println("3.Exit ");
    }

    public static void callLiftOptions(){
        System.out.println("What is your current Floor :");
        System.out.println("1.Floor 1 " );
        System.out.println("2.Floor 2 ");
        System.out.println("3.Floor 3");
        System.out.println("4.Floor 4");

    }
}
