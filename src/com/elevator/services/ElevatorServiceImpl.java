package ElevatorSystem.services;

import ElevatorSystem.entities.Elevator;
import ElevatorSystem.entities.Floor;

public class ElevatorServiceImpl implements  ElevatorService{
    private final Elevator elevator;

    public ElevatorServiceImpl( Elevator elevator){
        this.elevator = elevator;

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                elevator.move();
            }
        });
        t.start();
    }

    @Override
    public boolean callALift(Floor floor) {
        boolean status = floor.callLift();
        return status;
    }

    @Override
    public void goToAFloor(Floor floor) {
        this.elevator.addRequest(floor);
    }
}
