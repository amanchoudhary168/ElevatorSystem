package ElevatorSystem.services;

import ElevatorSystem.entities.Floor;

public interface ElevatorService {


    public boolean callALift(Floor floor);
    public void goToAFloor(Floor floor);


}
