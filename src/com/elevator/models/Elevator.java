package ElevatorSystem.entities;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

public class Elevator {

    enum Direction{
        UP,DOWN;
    }
    enum Status{
        MOVING,STOPPED;
    }

    String company;
    int capacity;

    volatile Floor currentFloor;
    volatile Direction currentDirection = Direction.UP;
    volatile Status currentStatus = Status.STOPPED;


    private Queue<Floor> upwardRequestQueue = new PriorityBlockingQueue<Floor>(10,
            (a, b) -> a.getFloorLevel() - b.getFloorLevel());
    private Queue<Floor> downwardRequestQueue = new PriorityBlockingQueue<Floor>(10,
            (a, b) -> b.getFloorLevel() - a.getFloorLevel());

    public Floor getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(Floor currentFloor) {
        this.currentFloor = currentFloor;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean addRequest(Floor floor){
        boolean requestQueued = false;

        if(currentDirection == Direction.UP){
           if(currentFloor.getFloorLevel() <= floor.getFloorLevel())
               requestQueued = upwardRequestQueue.add(floor);
           else
               requestQueued = downwardRequestQueue.add(floor);
        }
        if(currentDirection == Direction.DOWN){
            if(currentFloor.getFloorLevel() >= floor.getFloorLevel())
                requestQueued = downwardRequestQueue.add(floor);
            else
                requestQueued = upwardRequestQueue.add(floor);
        }

        return requestQueued;
    }

    public void move(){
        System.out.println("Move Started");
        while(true){
            if(upwardRequestQueue.size() == 0 && downwardRequestQueue.size() ==0 )
                currentStatus = Status.STOPPED;
            if(currentDirection == Direction.UP){
                while(!upwardRequestQueue.isEmpty()){
                    Floor floor = upwardRequestQueue.poll();
                    currentFloor = floor;
                    System.out.println("The lift has reached to Floor "+floor.floorLevel);
                }
                currentDirection =Direction.DOWN;
            }
            if(currentDirection == Direction.DOWN){
                while(!downwardRequestQueue.isEmpty()){
                    Floor floor = downwardRequestQueue.poll();
                    currentFloor=floor;
                    System.out.println("The lift has reached to Floor "+floor.floorLevel);
                }
                currentDirection =Direction.UP;
            }
            if(currentStatus == Status.STOPPED) {
                if (upwardRequestQueue.size() > 0) {
                    currentStatus = Status.MOVING;
                    currentDirection = Direction.UP;
                }
                if (downwardRequestQueue.size() > 0) {
                    currentStatus = Status.MOVING;
                    currentDirection = Direction.DOWN;
                }
            }

        }
    }







}
