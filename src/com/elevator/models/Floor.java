package ElevatorSystem.entities;

public class Floor {


    int floorLevel;
    String floorName;
    Elevator elevator;

    public Floor(int floorLevel, Elevator elevator) {
        this.floorLevel = floorLevel;
        this.elevator = elevator;
    }

    public Integer getFloorLevel() {
        return floorLevel;
    }

    public void setFloorLevel(Integer  floorLevel) {
        this.floorLevel = floorLevel;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }


    public boolean callLift(){
        return this.elevator.addRequest(this);
    }
}
